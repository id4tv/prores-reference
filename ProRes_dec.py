# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import sys
import time

import numpy as np
#import cv2
#import matplotlib.image
from functions import DC_decoding, AC_decoding
from functions import zigzag
from functions import idct_approx
#from functions import YUV2RGB

################################
##### Read bytes tmp file ######
################################

start = time.time()

## READ BITSTREAM
file = open("prores_frame100_1920x1080_10bit_422p.mkv", "rb")
byte = file.read()

## SKIP MKV METADATA
bytestream = byte[712:]

## FRAME HEADER
frame_header_size = int.from_bytes(bytestream[0:2], byteorder='big')
width = int.from_bytes(bytestream[8:10], byteorder='big')
height = int.from_bytes(bytestream[10:12], byteorder='big')


if (int(bin(bytestream[12])[2:4], 2) == 2):
    chroma_format = '422'
elif (int(bin(bytestream[12])[2:4], 2) == 3):
    chroma_format = '444'
else:
    print('Error: Not supported chroma format\n')
    sys.exit(1)
    
if (int(bin(bytestream[12])[6:8], 2) == 0):
    scan = 'progressive'
elif (int(bin(bytestream[12])[6:8], 2) == 1):
    scan = 'interlaced_top'
elif (int(bin(bytestream[12])[6:8], 2) == 2):
    scan = 'interlaced_bottom'
else:
    print('Error: Not supported scan type\n')
    sys.exit(1)
    
color_primaries = bytestream[14]
transfer_function = bytestream[15]
color_matrix = bytestream[16]

source_pixel_format = int(f'{int(hex(bytestream[17]), base=16):0>8b}'[0:4], 2)
alpha_info = int(f'{int(hex(bytestream[17]), base=16):0>8b}'[4:8], 2)

custom_luma_quant_matrix_present = int(f'{int(hex(bytestream[19]), base=16):0>8b}'[6], 2)
custom_chroma_quant_matrix_present = int(f'{int(hex(bytestream[19]), base=16):0>8b}'[6], 2)

QMatLuma = [None] * 64
if (custom_luma_quant_matrix_present):
    for i in range(64):
        QMatLuma[i] = bytestream[20 + i]
        
QMatChroma = [None] * 64
if (custom_chroma_quant_matrix_present):
    for i in range(64):
        QMatChroma[i] = bytestream[20 + 64 + i]
        
## PICUTRE HEADER
picture_header_size = bytestream[148]
picture_data_size = int(f'{int(hex(bytestream[149]), base=16):0>8b}' + 
                        f'{int(hex(bytestream[150]), base=16):0>8b}' + 
                        f'{int(hex(bytestream[151]), base=16):0>8b}' + 
                        f'{int(hex(bytestream[152]), base=16):0>8b}', 2)
total_slices = int(f'{int(hex(bytestream[153]), base=16):0>8b}' + f'{int(hex(bytestream[154]), base=16):0>8b}', 2)
slice_width_factor = pow(2, int(f'{int(hex(bytestream[155]), base=16):0>8b}'[0:4], 2))
slice_height_factor = pow(2, int(f'{int(hex(bytestream[155]), base=16):0>8b}'[4:8], 2))

## SLICE INDEX TABLE
slice_index_table = [None] * total_slices

for i in range(0, total_slices*2, 2):
    slice_index_table[int(i/2)] = hex(int.from_bytes(bytestream[156+i:156+i+2], byteorder='big'))
    
## SLICE HEADER
slice_header_size = [None] * total_slices
scale_factor = [None] * total_slices
luma_data_size = [None] * total_slices
u_data_size = [None] * total_slices
v_data_size = [None] * total_slices

slice_header_size[0] = bytestream[156+total_slices*2]
scale_factor[0] = bytestream[157+total_slices*2]
luma_data_size[0] = int.from_bytes(bytestream[158+total_slices*2:158+total_slices*2+2], byteorder='big')
u_data_size[0] = int.from_bytes(bytestream[160+total_slices*2:160+total_slices*2+2], byteorder='big')

## CODEWORD
codeword_size = len(bytestream[160+total_slices*2+2:])
codeword_tmp = bytestream[2202:]
codeword = [None] * codeword_size

for i in range(codeword_size):
    codeword[i] = hex(codeword_tmp[i])[2:]
    if (len(codeword[i]) == 1):
        codeword[i] = '0' + codeword[i]
    
end = time.time()
print('Elapsed time for reading proper bitstream (.mkv)', end - start)

print('Bytestream read successfully \u2713')

# Convert bytes into bits and store in bitstream array
bitstream = ''

for i in range(len(codeword)):
    bitstream = bitstream + f'{int(codeword[i], base=16):0>8b}'
    
bitstream_full = bitstream

########################################
##### Initiate Y, U, and V arrays ######
########################################

Y = []
U = []
V = []

for i in range(1020): #TODO: Later when your read slice index table, read also picture header
    Y.append([])
    
    for j in range(2):
        Y[i].append([])
        
        if ( j == 0):
            for k in range(32):
                Y[i][j].append([])
        else:
            for k in range(2016):
                Y[i][j].append([])
                
for i in range(1020): #TODO: Later when your read slice index table, read also picture header
    U.append([])
    
    for j in range(2):
        U[i].append([])
        
        if ( j == 0):
            for k in range(16):
                U[i][j].append([])
        else:
            for k in range(1008):
                U[i][j].append([])
                
for i in range(1020): #TODO: Later when your read slice index table, read also picture header
    V.append([])
    
    for j in range(2):
        V[i].append([])
        
        if ( j == 0):
            for k in range(16):
                V[i][j].append([])
        else:
            for k in range(1008):
                V[i][j].append([])
                
                
###########################
##### DC 1st SLICE Y ######
###########################

#blockPrint()

print('Entropy decoding...')

bitstream = bitstream_full
bs_org_size = len(bitstream)

DC = [None] * 32
dc_diff = [None] * 32

bitstream = DC_decoding(bitstream, DC, dc_diff, 32)

bs_size_after_dc = len(bitstream)

Y[0][0] = DC

#%pprint
#Y[0][0]

###########################
##### AC 1st SLICE Y ######
###########################

AC = [None] * 2016
run = [None] * 2016
level = [None] * 2016

bitstream = AC_decoding(bitstream[0:luma_data_size[0] * 8 - (bs_org_size - bs_size_after_dc)], AC, run, level)

idx = 0
for i in range(run.index(None)):
    if (run[i] != 0):
        for j in range(run[i]):
            AC[idx] = 0
            idx = idx + 1
    AC[idx] = level[i]
    idx = idx + 1
    
for i in range(AC.index(None), len(AC)):
    AC[i] = 0
    
Y[0][1] = AC

###########################
##### DC 1st SLICE U ######
###########################

DC = [None] * 16
dc_diff = [None] * 16

bitstream = DC_decoding(bitstream_full[luma_data_size[0] * 8:][0:u_data_size[0] * 8], DC, dc_diff, 16)

U[0][0] = DC

###########################
##### AC 1st SLICE U ######
###########################

AC = [None] * 1008
run = [None] * 1008
level = [None] * 1008

bitstream = AC_decoding(bitstream, AC, run, level)

idx = 0
for i in range(run.index(None)):
    if (run[i] != 0):
        for j in range(run[i]):
            AC[idx] = 0
            idx = idx + 1
    AC[idx] = level[i]
    idx = idx + 1
    
for i in range(AC.index(None), len(AC)):
    AC[i] = 0
    
U[0][1] = AC

###########################
##### DC 1st SLICE V ######
###########################

DC = [None] * 16
dc_diff = [None] * 16

v_data_size[0] = int(int('0194', 16) - luma_data_size[0] - u_data_size[0] - (slice_header_size[0] / 8))

bitstream = DC_decoding(bitstream_full[(luma_data_size[0] + u_data_size[0]) * 8:][0:v_data_size[0] * 8], DC, dc_diff, 16)

V[0][0] = DC

###########################
##### AC 1st SLICE V ######
###########################

AC = [None] * 1008
run = [None] * 1008
level = [None] * 1008

bitstream = AC_decoding(bitstream, AC, run, level)

idx = 0
for i in range(run.index(None)):
    if (run[i] != 0):
        for j in range(run[i]):
            AC[idx] = 0
            idx = idx + 1
    AC[idx] = level[i]
    idx = idx + 1
    
for i in range(AC.index(None), len(AC)):
    AC[i] = 0
    
V[0][1] = AC

####################################################
##### Read DCs and ACs for rest of the slices ######
####################################################

for i in range(1, 1020):
    
    # INITIATE BITSTREAM SLICE
    bitstream_slice = bitstream_full[sum(slice_header_size[1:i]) + (sum(luma_data_size[0:i]) + 
                                      sum(u_data_size[0:i]) + 
                                      sum(v_data_size[0:i])) * 8:][0:int(slice_index_table[i], 16) * 8]
    bitstream = bitstream_slice
    
    # READ SLICE HEADERS
    slice_header_size[i] = int(bitstream[0:8], 2)
    scale_factor[i] = int(bitstream[8:16], 2)
    luma_data_size[i] = int(bitstream[16:32], 2)
    u_data_size[i] = int(bitstream[32:48], 2)

    # DISCARD HEADER FROM BITSTREAM
    bitstream = bitstream[48:]
    
    #### DC Y ####

    DC = [None] * 32
    dc_diff = [None] * 32

    bs_org_size = len(bitstream)

    bitstream = DC_decoding(bitstream, DC, dc_diff, 32)

    bs_size_after_dc = len(bitstream)

    Y[i][0] = DC

    #### AC Y ####

    #blockPrint()

    AC = [None] * 2016
    run = [None] * 2016
    level = [None] * 2016

    bitstream = AC_decoding(bitstream[0:luma_data_size[i] * 8 - (bs_org_size - bs_size_after_dc)], AC, run, level)

    idx = 0
    for ii in range(run.index(None)):
        if (run[ii] != 0):
            for j in range(run[ii]):
                AC[idx] = 0
                idx = idx + 1
        AC[idx] = level[ii]
        idx = idx + 1

    if (None in AC):
        for ii in range(AC.index(None), len(AC)):
            AC[ii] = 0

    Y[i][1] = AC
    
    #### DC U ####
    DC = [None] * 16
    dc_diff = [None] * 16

    bitstream = DC_decoding(bitstream_slice[slice_header_size[i] + 
                                             luma_data_size[i] * 8:][0:u_data_size[i] * 8], DC, dc_diff, 16)

    U[i][0] = DC

    #### AC U ####
    AC = [None] * 1008
    run = [None] * 1008
    level = [None] * 1008

    bitstream = AC_decoding(bitstream, AC, run, level)

    idx = 0
    for ii in range(run.index(None)):
        if (run[ii] != 0):
            for j in range(run[ii]):
                AC[idx] = 0
                idx = idx + 1
        AC[idx] = level[ii]
        idx = idx + 1

    if (None in AC):
        for ii in range(AC.index(None), len(AC)):
            AC[ii] = 0

    U[i][1] = AC
    
    #### DC V ####
    DC = [None] * 16
    dc_diff = [None] * 16

    v_data_size[i] = int(int(slice_index_table[i], 16) - luma_data_size[i] - u_data_size[i] - (slice_header_size[i] / 8))

    bitstream = DC_decoding(bitstream_slice[slice_header_size[i] + 
                                             (luma_data_size[i] + 
                                              u_data_size[i]) * 8:][0:v_data_size[i] * 8], DC, dc_diff, 16)

    V[i][0] = DC

    #### AC 2nd SLICE V ####
    AC = [None] * 1008
    run = [None] * 1008
    level = [None] * 1008

    bitstream = AC_decoding(bitstream, AC, run, level)

    idx = 0
    for ii in range(run.index(None)):
        if (run[ii] != 0):
            for j in range(run[ii]):
                AC[idx] = 0
                idx = idx + 1
        AC[idx] = level[ii]
        idx = idx + 1

    if (None in AC):
        for ii in range(AC.index(None), len(AC)):
            AC[ii] = 0

    V[i][1] = AC
    
#     print('i:', i)
#     if (i == 1019):
#         print('Terminated successfully')

print('Dcs and ACs decoded successfully \u2713')


####################################################
##### Inverse block and slice scanning #############
####################################################

print('Inverse block and slice scanning...')

QF_Y = []
QF_U = []
QF_V = []

for i in range(1020):
    QF_Y.append([])
    
    for j in range(2048):
        QF_Y[i].append([])
            
for i in range(1020):
    QF_U.append([])
    QF_V.append([])
    
    for j in range(1024):
        QF_U[i].append([])
        QF_V[i].append([])
            
for i in range(1020):
        QF_Y[i][0:32] = Y[i][0]
        QF_Y[i][32:] = Y[i][1]
            
for i in range(1020):
        QF_U[i][0:16] = U[i][0]
        QF_U[i][16:] = U[i][1]
        QF_V[i][0:16] = V[i][0]
        QF_V[i][16:] = V[i][1]

####################################################

qcoeffs_Y = []
qcoeffs_U = []
qcoeffs_V = []

for i in range(1020):
    qcoeffs_Y.append([])
    
    for j in range(32):
        qcoeffs_Y[i].append([])
        
        for k in range(64):
            qcoeffs_Y[i][j].append([])
            
for i in range(1020):
    qcoeffs_U.append([])
    qcoeffs_V.append([])
    
    for j in range(16):
        qcoeffs_U[i].append([])
        qcoeffs_V[i].append([])
        
        for k in range(64):
            qcoeffs_U[i][j].append([])
            qcoeffs_V[i][j].append([])

for i in range(1020):
    
    for j in range(32):
        
        block = [None] * 64
        cnt = 0
        
        for k in range(64):
            
            block[k] = QF_Y[i][cnt + j]
            cnt = cnt + 32
        
        qcoeffs_Y[i][j] = block
        
for i in range(1020):
    
    for j in range(16):
        
        blockU = [None] * 64
        blockV = [None] * 64
        cnt = 0
        
        for k in range(64):
            
            blockU[k] = QF_U[i][cnt + j]
            blockV[k] = QF_V[i][cnt + j]
            cnt = cnt + 16
        
        qcoeffs_U[i][j] = blockU
        qcoeffs_V[i][j] = blockV
        
for i in range(1020):
    
    for j in range(32):
        
        scanned = [None] * 64
        
        for k in range(64):
            
            scanned[k] = qcoeffs_Y[i][j][zigzag[k]]
            
        qcoeffs_Y[i][j] = scanned
        
for i in range(1020):
    
    for j in range(16):
        
        scannedU = [None] * 64
        scannedV = [None] * 64
        
        for k in range(64):
            
            scannedU[k] = qcoeffs_U[i][j][zigzag[k]]
            scannedV[k] = qcoeffs_V[i][j][zigzag[k]]
            
        qcoeffs_U[i][j] = scannedU
        qcoeffs_V[i][j] = scannedV
        
        
print('Inverse block and slice scanning performed successfully \u2713')

########################################
##### Inverse Quantization #############
########################################

print('Inverse quantization...')

ffcoeffs_Y = []
ffcoeffs_U = []
ffcoeffs_V = []

for i in range(1020):
    ffcoeffs_Y.append([])
    
    for j in range(32):
        ffcoeffs_Y[i].append([])
        
        for k in range(64):
            ffcoeffs_Y[i][j].append([])
            
for i in range(1020):
    ffcoeffs_U.append([])
    ffcoeffs_V.append([])
    
    for j in range(16):
        ffcoeffs_U[i].append([])
        ffcoeffs_V[i].append([])
        
        for k in range(64):
            ffcoeffs_U[i][j].append([])
            ffcoeffs_V[i][j].append([])
    
#TODO: Review iQuant, currently multiplying with qscale yields best result            
    
for i in range(1020):
    for j in range(32):
        for k in range(64):
            if (k == 0):
                ffcoeffs_Y[i][j][k] = (qcoeffs_Y[i][j][k]) * scale_factor[i]
                #ffcoeffs_Y[i][j][k] = (qcoeffs_Y[i][j][k] + 4096) * scale_factor[i] * 4 / 8
                #ffcoeffs_Y[i][j][k] = (qcoeffs_Y[i][j][k] + 4096) * scale_factor[i] * QmatLuma[k]
            else:
                ffcoeffs_Y[i][j][k] = qcoeffs_Y[i][j][k] * scale_factor[i]
                #ffcoeffs_Y[i][j][k] = qcoeffs_Y[i][j][k] * scale_factor[i] * QmatLuma[k]
    
    for l in range(16):
        for m in range(64):
            if (m == 0):
                #ffcoeffs_U[i][l][m] = (qcoeffs_U[i][l][m] + 4096) * scale_factor[i] * 4 / 8
                #ffcoeffs_V[i][l][m] = (qcoeffs_V[i][l][m] + 4096) * scale_factor[i] * 4 / 8 
                ffcoeffs_U[i][l][m] = (qcoeffs_U[i][l][m]) * scale_factor[i]
                ffcoeffs_V[i][l][m] = (qcoeffs_V[i][l][m]) * scale_factor[i]
                #ffcoeffs_U[i][l][m] = (qcoeffs_U[i][l][m] + 4096) * scale_factor[i] * QmatChroma[k]
                #ffcoeffs_V[i][l][m] = (qcoeffs_V[i][l][m] + 4096) * scale_factor[i] * QmatChroma[k]
            else:
                ffcoeffs_U[i][l][m] = qcoeffs_U[i][l][m] * scale_factor[i]
                ffcoeffs_V[i][l][m] = qcoeffs_V[i][l][m] * scale_factor[i]
                #ffcoeffs_U[i][l][m] = qcoeffs_U[i][l][m] * scale_factor[i] * QmatChroma[k]
                #ffcoeffs_V[i][l][m] = qcoeffs_V[i][l][m] * scale_factor[i] * QmatChroma[k]
                
print('Inverse quantization performed successfully \u2713')


#####################################################
##### Inverse Discrete Cosine Transform #############
#####################################################

##### Initiate

pixels_Y = []
pixels_U = []
pixels_V = []

for i in range(1020):
    pixels_Y.append([])
    
    for j in range(32):
        pixels_Y[i].append([])
        
        for k in range(64):
            pixels_Y[i][j].append([])
            
for i in range(1020):
    pixels_U.append([])
    pixels_V.append([])
    
    for j in range(16):
        pixels_U[i].append([])
        pixels_V[i].append([])
        
        for k in range(64):
            pixels_U[i][j].append([])
            pixels_V[i][j].append([])
        
print('Inverse DCT initiation \u2713')        
        
##### DCT Y

print('Inverse DCT for Y channel...')
            
iout = []

for i in range(8):
    iout.append([])
    
    for j in range(8):
        iout[i].append([])
        
for slices in range(1020):
    
    for blocks in range(32):
        
        out = np.array(ffcoeffs_Y[slices][blocks]).reshape((8, 8))
            
        iout = idct_approx(out)
        
        for i in range(8):
            for j in range(8):
                pixels_Y[slices][blocks][i * 8 + j] = round(iout[i][j])

print('Inverse DCT channel Y \u2713')
                
##### DCT U

print('Inverse DCT for channel U...')

iout = []

for i in range(8):
    iout.append([])
    
    for j in range(8):
        iout[i].append([])
        
for slices in range(1020):
    
    for blocks in range(16):
        
        out = np.array(ffcoeffs_U[slices][blocks]).reshape((8, 8))
        
        iout = idct_approx(out)        
        
        for i in range(8):
            for j in range(8):
                pixels_U[slices][blocks][i * 8 + j] = round(iout[i][j])

print('Inverse DCT channel U \u2713')

##### DCT V

print('Inverse DCT for channel V...')

iout = []

for i in range(8):
    iout.append([])
    
    for j in range(8):
        iout[i].append([])
        
for slices in range(1020):
    
    for blocks in range(16):
        
        out = np.array(ffcoeffs_V[slices][blocks]).reshape((8, 8))
        
        iout = idct_approx(out)
            
        iout = out
        for i in range(8):
            for j in range(8):
                pixels_V[slices][blocks][i * 8 + j] = round(iout[i][j])
                
print('Inverse DCT channel V \u2713')


###################################
##### Organize slices #############
###################################

##### Initiate slices

print('Re-arranging MBs and slices to reconstruct the frame...')

slices_Y = []
slices_U = []
slices_V = []

for s in range(1020):
    slices_Y.append([])
    
    for i in range(8):
        slices_Y[s].append([])

        for j in range(16):
            slices_Y[s][i].append([])

            for k in range(16):
                slices_Y[s][i][j].append([])

for s in range(1020):
    slices_U.append([])
    slices_V.append([])
    
    for i in range(8):
        slices_U[s].append([])
        slices_V[s].append([])

        for j in range(16):
            slices_U[s][i].append([])
            slices_V[s][i].append([])

            for k in range(8):
                slices_U[s][i][j].append([])
                slices_V[s][i][j].append([])
                
#### Re-arrange luma pixels

for s in range(1020):

    for i in range(8):

        slices_Y[s][i][0][0:8] = pixels_Y[s][i * 4][0:8]
        slices_Y[s][i][1][0:8] = pixels_Y[s][i * 4][8:16]
        slices_Y[s][i][2][0:8] = pixels_Y[s][i * 4][16:24]
        slices_Y[s][i][3][0:8] = pixels_Y[s][i * 4][24:32]
        slices_Y[s][i][4][0:8] = pixels_Y[s][i * 4][32:40]
        slices_Y[s][i][5][0:8] = pixels_Y[s][i * 4][40:48]
        slices_Y[s][i][6][0:8] = pixels_Y[s][i * 4][48:56]
        slices_Y[s][i][7][0:8] = pixels_Y[s][i * 4][56:64]

        slices_Y[s][i][0][8:16] = pixels_Y[s][i * 4 + 1][0:8]
        slices_Y[s][i][1][8:16] = pixels_Y[s][i * 4 + 1][8:16]
        slices_Y[s][i][2][8:16] = pixels_Y[s][i * 4 + 1][16:24]
        slices_Y[s][i][3][8:16] = pixels_Y[s][i * 4 + 1][24:32]
        slices_Y[s][i][4][8:16] = pixels_Y[s][i * 4 + 1][32:40]
        slices_Y[s][i][5][8:16] = pixels_Y[s][i * 4 + 1][40:48]
        slices_Y[s][i][6][8:16] = pixels_Y[s][i * 4 + 1][48:56]
        slices_Y[s][i][7][8:16] = pixels_Y[s][i * 4 + 1][56:64]

        slices_Y[s][i][8][0:8] = pixels_Y[s][i * 4 + 2][0:8]
        slices_Y[s][i][9][0:8] = pixels_Y[s][i * 4 + 2][8:16]
        slices_Y[s][i][10][0:8] = pixels_Y[s][i * 4 + 2][16:24]
        slices_Y[s][i][11][0:8] = pixels_Y[s][i * 4 + 2][24:32]
        slices_Y[s][i][12][0:8] = pixels_Y[s][i * 4 + 2][32:40]
        slices_Y[s][i][13][0:8] = pixels_Y[s][i * 4 + 2][40:48]
        slices_Y[s][i][14][0:8] = pixels_Y[s][i * 4 + 2][48:56]
        slices_Y[s][i][15][0:8] = pixels_Y[s][i * 4 + 2][56:64]

        slices_Y[s][i][8][8:16] = pixels_Y[s][i * 4 + 3][0:8]
        slices_Y[s][i][9][8:16] = pixels_Y[s][i * 4 + 3][8:16]
        slices_Y[s][i][10][8:16] = pixels_Y[s][i * 4 + 3][16:24]
        slices_Y[s][i][11][8:16] = pixels_Y[s][i * 4 + 3][24:32]
        slices_Y[s][i][12][8:16] = pixels_Y[s][i * 4 + 3][32:40]
        slices_Y[s][i][13][8:16] = pixels_Y[s][i * 4 + 3][40:48]
        slices_Y[s][i][14][8:16] = pixels_Y[s][i * 4 + 3][48:56]
        slices_Y[s][i][15][8:16] = pixels_Y[s][i * 4 + 3][56:64]
        
#### Re-arrange chroma pixels

for s in range(1020):

    for i in range(8):

        ##U
        
        slices_U[s][i][0][0:8] = pixels_U[s][i * 2][0:8]
        slices_U[s][i][1][0:8] = pixels_U[s][i * 2][8:16]
        slices_U[s][i][2][0:8] = pixels_U[s][i * 2][16:24]
        slices_U[s][i][3][0:8] = pixels_U[s][i * 2][24:32]
        slices_U[s][i][4][0:8] = pixels_U[s][i * 2][32:40]
        slices_U[s][i][5][0:8] = pixels_U[s][i * 2][40:48]
        slices_U[s][i][6][0:8] = pixels_U[s][i * 2][48:56]
        slices_U[s][i][7][0:8] = pixels_U[s][i * 2][56:64]
        
        slices_U[s][i][8][0:8] = pixels_U[s][i * 2 + 1][0:8]
        slices_U[s][i][9][0:8] = pixels_U[s][i * 2 + 1][8:16]
        slices_U[s][i][10][0:8] = pixels_U[s][i * 2 + 1][16:24]
        slices_U[s][i][11][0:8] = pixels_U[s][i * 2 + 1][24:32]
        slices_U[s][i][12][0:8] = pixels_U[s][i * 2 + 1][32:40]
        slices_U[s][i][13][0:8] = pixels_U[s][i * 2 + 1][40:48]
        slices_U[s][i][14][0:8] = pixels_U[s][i * 2 + 1][48:56]
        slices_U[s][i][15][0:8] = pixels_U[s][i * 2 + 1][56:64]
        
        ##V
        
        slices_V[s][i][0][0:8] = pixels_V[s][i * 2][0:8]
        slices_V[s][i][1][0:8] = pixels_V[s][i * 2][8:16]
        slices_V[s][i][2][0:8] = pixels_V[s][i * 2][16:24]
        slices_V[s][i][3][0:8] = pixels_V[s][i * 2][24:32]
        slices_V[s][i][4][0:8] = pixels_V[s][i * 2][32:40]
        slices_V[s][i][5][0:8] = pixels_V[s][i * 2][40:48]
        slices_V[s][i][6][0:8] = pixels_V[s][i * 2][48:56]
        slices_V[s][i][7][0:8] = pixels_V[s][i * 2][56:64]
        
        slices_V[s][i][8][0:8] = pixels_V[s][i * 2 + 1][0:8]
        slices_V[s][i][9][0:8] = pixels_V[s][i * 2 + 1][8:16]
        slices_V[s][i][10][0:8] = pixels_V[s][i * 2 + 1][16:24]
        slices_V[s][i][11][0:8] = pixels_V[s][i * 2 + 1][24:32]
        slices_V[s][i][12][0:8] = pixels_V[s][i * 2 + 1][32:40]
        slices_V[s][i][13][0:8] = pixels_V[s][i * 2 + 1][40:48]
        slices_V[s][i][14][0:8] = pixels_V[s][i * 2 + 1][48:56]
        slices_V[s][i][15][0:8] = pixels_V[s][i * 2 + 1][56:64]
        
################################################
##### Organize slices into a frame #############
################################################

#### Re-arrange Y channel MBs

MB_row_Y = []

for i in range(68):
    MB_row_Y.append([0])
    
for row in range(68):
    
    slice_tmp = []

    for i in range(15):
        slice_tmp.append([])
    
    for i in range(15):

        slice_tmp[i] = np.concatenate((np.array(slices_Y[i+15*row][0]), np.array(slices_Y[i+15*row][1]), 
                                       np.array(slices_Y[i+15*row][2]), np.array(slices_Y[i+15*row][3]), 
                                       np.array(slices_Y[i+15*row][4]), np.array(slices_Y[i+15*row][5]), 
                                       np.array(slices_Y[i+15*row][6]), np.array(slices_Y[i+15*row][7])), axis = 1)
                                      
    MB_row_Y[row] = np.concatenate((slice_tmp[0], slice_tmp[1], slice_tmp[2], slice_tmp[3], slice_tmp[4],
                                  slice_tmp[5], slice_tmp[6], slice_tmp[7], slice_tmp[8], slice_tmp[9],
                                  slice_tmp[10], slice_tmp[11], slice_tmp[12], slice_tmp[13], slice_tmp[14]), axis = 1)
    
#### Re-arrange U channel MBs

MB_row_U = []

for i in range(68):
    MB_row_U.append([0])
    
for row in range(68):
    
    slice_tmp = []

    for i in range(15):
        slice_tmp.append([])
    
    for i in range(15):

        slice_tmp[i] = np.concatenate((np.array(slices_U[i+15*row][0]), np.array(slices_U[i+15*row][1]), 
                                      np.array(slices_U[i+15*row][2]), np.array(slices_U[i+15*row][3]), 
                                      np.array(slices_U[i+15*row][4]), np.array(slices_U[i+15*row][5]), 
                                      np.array(slices_U[i+15*row][6]), np.array(slices_U[i+15*row][7])), axis = 1)
                                      
    MB_row_U[row] = np.concatenate((slice_tmp[0], slice_tmp[1], slice_tmp[2], slice_tmp[3], slice_tmp[4],
                                  slice_tmp[5], slice_tmp[6], slice_tmp[7], slice_tmp[8], slice_tmp[9],
                                  slice_tmp[10], slice_tmp[11], slice_tmp[12], slice_tmp[13], slice_tmp[14]), axis = 1)

#### Re-arrange V channel MBs

MB_row_V = []

for i in range(68):
    MB_row_V.append([0])
    
for row in range(68):
    
    slice_tmp = []

    for i in range(15):
        slice_tmp.append([])
    
    for i in range(15):

        slice_tmp[i] = np.concatenate((np.array(slices_V[i+15*row][0]), np.array(slices_V[i+15*row][1]), 
                                       np.array(slices_V[i+15*row][2]), np.array(slices_V[i+15*row][3]), 
                                       np.array(slices_V[i+15*row][4]), np.array(slices_V[i+15*row][5]), 
                                       np.array(slices_V[i+15*row][6]), np.array(slices_V[i+15*row][7])), axis = 1)
                                      
    MB_row_V[row] = np.concatenate((slice_tmp[0], slice_tmp[1], slice_tmp[2], slice_tmp[3], slice_tmp[4],
                                  slice_tmp[5], slice_tmp[6], slice_tmp[7], slice_tmp[8], slice_tmp[9],
                                  slice_tmp[10], slice_tmp[11], slice_tmp[12], slice_tmp[13], slice_tmp[14]), axis = 1)
                                  
 
#####################################
##### Reconstruct frame #############
#####################################

#YUV = cv2.merge([Y_array, U_array, V_array])

print('Frame reconstructed \u2713')

#####################################
##### Write YUV to file #############
#####################################

print('Writing frame to file...')

Y_array = np.array(MB_row_Y).reshape(1088 * 1920) + 512
U_array = np.array(MB_row_U).reshape(1088 * 960) + 512
V_array = np.array(MB_row_V).reshape(1088 * 960) + 512

YUV = np.concatenate((Y_array, U_array, V_array))
YUV = np.array(YUV, dtype = np.int16)

f = open('prores_frame100_1920x1088_10bit_422p - reconstructed.yuv', 'wb')
f.write(YUV)
f.close()

#RGB = YUV2RGB(YUV[0:1080,:,:])

#####################################################################
##### Write reconstructed frame to a .jpg visualization #############
#####################################################################

#RGB_norm = cv2.normalize(RGB, dst=None, alpha=0.0, beta=1.0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
#RGB_norm = RGB_norm.clip(min=0)
#RGB_norm = RGB_norm.clip(max=1)
#matplotlib.image.imsave('reconstructed_frame.jpg', RGB_norm)

print('Reconstructed YUV saved successfully \u2713')

#BGR = cv2.merge([RGB[:,:,2], RGB[:,:,1], RGB[:,:,0]])

####VISUALIZE FRAME
#cv2.imshow('TEST', cv2.normalize(BGR, dst=None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F))
#cv2.waitKey(0)
#cv2.destroyAllWindows()