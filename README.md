# README #

ProRes decoding (WORK IN PROGRESS)

### NOTES ###

* python non-optimized code to be used as reference for the development of the ProRes and ProResGPU repos
* run ProRes_dec.py to read an apple prores bitstream stored in an .mkv container to get the reconstructed (decoded) YUV frame

### PREREQUISITES ###

* numpy      (pip install numpy)