# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 10:13:21 2021

@author: Admin
"""

import sys, os
import math
import numpy as np

# TABLES

zigzag = [0,  1,  4,  5,  16,  17,  21,  22,
          2,  3,  6,  7,  18,  20,  23,  28,
          8,  9,  12, 13, 19,  24,  27,  29,
         10,  11, 14, 15, 25,  26,  30,  31,
         32,  33, 37, 38, 45,  46,  53,  54,
         34,  36, 39, 44, 47,  52,  55,  60,
         35,  40, 43, 48, 51,  56,  59,  61,
         41,  42, 49, 50, 57,  58,  62,  63]

QmatLuma = [4, 4, 4, 4, 4, 4, 4, 4,
            4, 4, 4, 4, 4, 4, 4, 4,
            4, 4, 4, 4, 4, 4, 4, 4,
            4, 4, 4, 4, 4, 4, 4, 5,
            4, 4, 4, 4, 4, 4, 5, 5,
            4, 4, 4, 4, 4, 5, 5, 6,
            4, 4, 4, 4, 5, 5, 6, 7,
            4, 4, 4, 4, 5, 6, 7, 7,]

QmatChroma = [4, 4, 4, 4, 4, 4, 4, 4,
              4, 4, 4, 4, 4, 4, 4, 4,
              4, 4, 4, 4, 4, 4, 4, 4,
              4, 4, 4, 4, 4, 4, 4, 5,
              4, 4, 4, 4, 4, 4, 5, 5,
              4, 4, 4, 4, 4, 5, 5, 6,
              4, 4, 4, 4, 5, 5, 6, 7,
              4, 4, 4, 4, 5, 6, 7, 7,]

# FUNCTIONS
# 1. Golomb Rice ENC
# 2. Golomb Rice DEC
# 3. Exp Golomb ENC
# 4. Exp Golomb DEC
# 5. Rice Golomb Exp Combo ENC
# 6. Rice Golomb Exp Combo DEC
# 7. Signed int to symbol conv
# 8. Inverse signed symbol to int conv

def Golomb_encoding(n, k):

    q = math.floor(n / math.pow(2,k))
    r = n % pow(2,k)

    #codeword_size = q + 1 + k
    codeword = []

    for i in range(q):
        codeword.append(0)
    
    codeword.append(1)

    r_bin = bin(r)

    for i in range(k - len(r_bin[2:])):
        codeword.append(0)
    
    if (r != 0):
        for i in range(len(r_bin[2:])):
               codeword.append(int(r_bin[2:][i]))
        
    codeword_str = str(codeword[0])
    
    for i in range(1, len(codeword)):
        codeword_str = codeword_str + str(codeword[i])
    
    return codeword_str

# ----

def Golomb_decoding(codeword_str, k):
    
    codeword = []
    
    for i in range(len(codeword_str)):
        codeword.append(int(codeword_str[i]))
    
    Q = 0

    for i in range(len(codeword)):
        if codeword[i] == 0:
            Q = Q + 1
        elif codeword[i] == 1:
            break
            
    R = codeword[(len(codeword) - k):]
    
    if (R):
        R_str = str(R[0])
    
        for i in range(1, len(R)):
            R_str = R_str + str(R[i])
        
        N = Q * pow(2, k) + int(R_str, 2)
    else:
        N = Q * pow(2, k)
    
    return N

# ----

def Exp_golomb_encoding(n, k):

    q = math.floor( math.log(( n + pow(2, k) ),2)) - k
    r = n + pow(2, k) - pow(2, q+k)

    codeword = []

    for i in range(q):
        codeword.append(0)
    
    codeword.append(1)

    r_bin = bin(r)

    for i in range((q+k) - len(r_bin[2:])):
        codeword.append(0)
    
    for i in range(len(r_bin[2:])):
        codeword.append(int(r_bin[2:][i]))
        
    codeword_str = str(codeword[0])
    
    for i in range(1, len(codeword)):
        codeword_str = codeword_str + str(codeword[i])
    
    return codeword_str

# ----

def Exp_golomb_decoding(codeword_str, k):
    
    return int(codeword_str, 2) - pow(2, k)

# ----

def Rice_golomb_exp_combo_encoding(n, lastRiceQ, Krice, Kexp):
    
    if (n < (lastRiceQ + 1) * pow(2,Krice)):
        return Golomb_encoding(n, Krice)
    else:
        codeword = ''
        
        for i in range(lastRiceQ + 1):
            codeword = codeword + '0'
        
        codeword = codeword + Exp_golomb_encoding((n - (lastRiceQ + 1) * pow(2, Krice)), Kexp)
        
        return codeword

# ----
    
def Rice_golomb_exp_combo_decoding(codeword_str, lastRiceQ, Krice, Kexp):
    
    Q = 0

    for i in range(len(codeword_str)):
        if codeword_str[i] == '0':
            Q = Q + 1
        elif codeword_str[i] == '1':
            break
            
    if (Q <= lastRiceQ):
        return Golomb_decoding(codeword_str, Krice)
    
    else:
        codeword_str = codeword_str[lastRiceQ + 1:]
        N = Exp_golomb_decoding(codeword_str, Kexp)
        return N + (lastRiceQ + 1) * pow(2, Krice)

# ----    
    
def Signed_golomb_comb_codes(n):
    
    if n >= 0:
        return 2 * abs(n)
    elif n < 0:
        return 2 * abs(n) - 1

# ----    
    
def Inverse_signed_golomb_comb_codes(n):
    
    if (n % 2) == 0:
        return n / 2
    elif (n % 2) != 0:
        return -((n + 1) / 2)
    


###########################
###########################
# AC RUN AND LEVEL TABLES


def define_codebook_run(previousRun):
    
    previousRun = abs(previousRun)
    
    lastRiceQ = 0
    Krice = 0
    Kexp = 0
    codebook = ''
    
    if (previousRun == 0 or previousRun == 1):
        lastRiceQ = 2
        Krice = 0
        Kexp = 1
        codebook = 'combo'
    elif (previousRun == 2 or previousRun == 3):
        lastRiceQ = 1
        Krice = 0
        Kexp = 1
        codebook = 'combo'
    elif (previousRun == 4):
        Kexp = 0
        codebook = 'exp'
    elif (previousRun == 5 or previousRun == 6 
          or previousRun == 7 or previousRun == 8):  
        lastRiceQ = 1
        Krice = 1
        Kexp = 2
        codebook = 'combo'
    elif (previousRun == 9 or previousRun == 10 
          or previousRun == 11 or previousRun == 12 
          or previousRun == 13 or previousRun == 14):
        Kexp = 1
        codebook = 'exp'
    elif (previousRun >= 15):
        Kexp = 2
        codebook = 'exp'
    else:
        print('Error: previousRun cannot be negative')
        return
        
    return lastRiceQ, Krice, Kexp, codebook

def define_codebook_level(previousLevel):
    
    lastRiceQ = 0
    Krice = 0
    Kexp = 0
    codebook = ''
    
    if (previousLevel == 0):
        lastRiceQ = 2
        Krice = 0
        Kexp = 2
        codebook = 'combo'
    elif (previousLevel == 1):
        lastRiceQ = 1
        Krice = 0
        Kexp = 1
        codebook = 'combo'
    elif (previousLevel == 2):
        lastRiceQ = 2
        Krice = 0
        Kexp = 1
        codebook = 'combo'
    elif (previousLevel == 3):
        Kexp = 0
        codebook = 'exp'
    elif (previousLevel == 4 or previousLevel == 5 or previousLevel == 6 or previousLevel == 7):
        Kexp = 1
        codebook = 'exp'
    elif (previousLevel >= 8):
        Kexp = 2
        codebook = 'exp'
    else:
        print('Error: previousLevel cannot be negative')
        return
        
    return lastRiceQ, Krice, Kexp, codebook



### HELPER FUNCTIONS ###
# 1. Disable print
# 2. Enable print


def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__
    
    
#########################
#########################
#########################


def DC_decoding(bitstream, DC, dc_diff, num_coeffs):
    
    #new_dc_diff = [None] * num_coeffs
    
    for i in range(num_coeffs):
    
        q = bitstream.index('1')
        #print('q =', q)

        if (i == 0):
            k = 5
        elif (i == 1):
            k = 3      

        #print('k =', k)
        #if bitstream != '0000':

        if (k == 2):
            if (q <= 1):
                #print('#### We enter GOLOMB ####')
                codeword_size = q + 1 + 2
                #print('first (calculated) codeword_size for k = 2: ', codeword_size)
                
                dec_value = Golomb_decoding(bitstream[0:codeword_size], 2)
                #print('first (calculated) dec_value for k = 2: ', dec_value)
                
                dc_diff[i] = Inverse_signed_golomb_comb_codes(dec_value)
                #print('first dc_diff for k = 2: ', dc_diff[i])

                #print(bitstream[0:codeword_size])
                #bitstream = bitstream[codeword_size:]
            else:
                #print('ALERT: ---------- ###################################')
                #print('#### We enter EXP ####')
                bitstream = bitstream[1 + 1:]

                q = bitstream.index('1')
                #print('q: ', q)
                
                codeword_size = 2 * q + 1 + 3
                #print('first (calculated) codeword_size for K = 2: ', codeword_size)

                N = Exp_golomb_decoding(bitstream[0:codeword_size], 3)
                #print('first (calculated) N for k = 2: ', N)

                dec_value = N + ((1 + 1) * pow(2, 2))
                #print('first (calculated) dec_value for k = 2: ', dec_value)

                dc_diff[i] = Inverse_signed_golomb_comb_codes(dec_value)
                #print('first level: ', dc_diff[i])

                #print(bitstream[0:codeword_size])
                #bitstream = bitstream[codeword_size:]

        else:
            #print('#### We enter EXP ####')
            codeword_size = 2 * q + 1 + k
            #print('codeword size =', codeword_size)
            
            #print(bitstream[0:codeword_size])
            dec_value = Exp_golomb_decoding(bitstream[0:codeword_size], k)
            #print('Exp Golomb decoded value is : ', dec_value)

            dc_diff[i] = Inverse_signed_golomb_comb_codes(dec_value)
            #print('dc_diff: ', dc_diff[i])

        if (i < 1):
            DC[i] = dc_diff[i]
            
            #print('i: ', i, ' -------------------')
            #print('k: ', k)
            #print('q: ', q)
            #print('codeword_size: ', codeword_size)
            #print('codeword:', bitstream[0:codeword_size])
            #print('first dc_diff: ', i,  dc_diff[i])
            #print('first DC: ', i,  DC[i])
        else:
            if (i != 1 and dc_diff[i - 1] < 0):
                dc_diff[i] = -dc_diff[i]
            
            DC[i] = DC[i - 1] + dc_diff[i]
            
            #print('i: ', i, ' -------------------')
            #print('k: ', k)
            #print('q: ', q)
            #print('codeword_size: ', codeword_size)
            #print('codeword:', bitstream[0:codeword_size])
            #print('dc_diff: ', i,  dc_diff[i])
            #print('DC: ', i,  DC[i])

            if (abs(dc_diff[i]) == 0):
                k = 0
            elif (abs(dc_diff[i]) == 1):
                k = 1
            elif (abs(dc_diff[i]) == 2):
                k = 2
            else:
                k = 3

        #print(bitstream[0:codeword_size])
        bitstream = bitstream[codeword_size:]
        #print(codeword_size)
        #print(bitstream)
    
    return bitstream 


def AC_decoding(bitstream, AC, run, level):

    i = 0
    
    while(bitstream and '1' in bitstream):

        if (i == 0):
            #print('#### We enter First RUN ####')
            ### FIRST RUN ###
            q = bitstream.index('1')
            #print('First q: ', q)

            previousRun = 4
            #print('first (default) run: ', previousRun)

            lastRiceQ, Krice, Kexp, codebook = define_codebook_run(previousRun)
            #print('first (default) lastRiceQ, Krice, Kexp, codebook for Run: ', lastRiceQ, Krice, Kexp, codebook)

            #print('#### We enter first (default) EXP ####')

            codeword_size = 2 * q + 1 + Krice
            #print('first (calculated) codeword_size for run: ', codeword_size)

            run[i] = Exp_golomb_decoding(bitstream[0:codeword_size], Kexp)
            #print('first (calculated) run: ', run[i])

            bitstream = bitstream[codeword_size:]

            ### FIRST LEVEL ###
            #print('#### We enter First LEVEL ####')
            q = bitstream.index('1')
            #print('First q: ', q)

            previousLevel = 2
            #print('first (default) level: ', previousLevel)
            previousLevel = abs(previousLevel) - 1
            #print('first |previousLevel| - 1: ', previousLevel)
            lastRiceQ, Krice, Kexp, codebook = define_codebook_level(previousLevel)
            #print('first (default) lastRiceQ, Krice, Kexp, codebook for Level: ', lastRiceQ, Krice, Kexp, codebook)

            if (q <= lastRiceQ):
                #print('#### We enter first (default) COMBO SMALL ####')
                codeword_size = q + 1 + Krice
                #print('first (calculated) codeword_size for level: ', codeword_size)

                abs_level_minus_1 = Golomb_decoding(bitstream[0:codeword_size], Krice)
                #print('first (calculated) abs_level_minus_1: ', abs_level_minus_1)

                sign = int(bitstream[codeword_size])
                #print('first sign: ', sign)

                level[i] = (abs_level_minus_1 + 1) * (1 - 2 * sign)
                #print('first level: ', level[i])

                bitstream = bitstream[codeword_size + 1:]
                i = i + 1
            else:
                #print('#### We enter first (default) COMBO LARGE ####')
                bitstream = bitstream[lastRiceQ + 1:]

                q = bitstream.index('1')
                #print('First q: ', q)
                
                codeword_size = 2 * q + 1 + Kexp
                #print('first (calculated) codeword_size for level: ', codeword_size)

                N = Exp_golomb_decoding(bitstream[0:codeword_size], Kexp)
                #print('first (calculated) N for level: ', N)

                abs_level_minus_1 = N + ((lastRiceQ + 1) * pow(2,Krice))
                #print('first (calculated) abs_level_minus_1: ', abs_level_minus_1)

                sign = int(bitstream[codeword_size])
                #print('first sign: ', sign)

                level[i] = (abs_level_minus_1 + 1) * (1 - 2 * sign)
                #print('first level: ', level[i])

                bitstream = bitstream[codeword_size + 1:]
                i = i + 1
        else:

            ### RUN ###
            #print('#### We enter RUN ####')
            q = bitstream.index('1')
            #print('q ', i, ': ', q)

            previousRun = run[i - 1]
            #print('previousRun ', i, ': ', previousRun)

            lastRiceQ, Krice, Kexp, codebook = define_codebook_run(previousRun)
            #print('lastRiceQ, Krice, Kexp, codebook ', i, ': ', lastRiceQ, Krice, Kexp, codebook)

            if (codebook == 'exp'):
                #print('#### We enter EXP for RUN ####')
                codeword_size = 2 * q + 1 + Kexp
                #print('codeword_size ', i, ': ', codeword_size)
                run[i] = Exp_golomb_decoding(bitstream[0:codeword_size], Kexp)
                #print('run ', i, ': ', run[i])
                bitstream = bitstream[codeword_size:]
            elif (codebook == 'combo'):
                if (q <= lastRiceQ):
                    #print('#### We enter COMBO SMALL for RUN ####')
                    codeword_size = q + 1 + Krice
                    #print('codeword_size ', i, ': ',codeword_size)
                    run[i] = Golomb_decoding(bitstream[0:codeword_size], Krice)
                    #print('run ', i, ': ', run[i])
                    bitstream = bitstream[codeword_size:]
                else:
                    #print('#### We enter COMBO LARGE for RUN ####')
                    bitstream = bitstream[lastRiceQ + 1:]
                    q = bitstream.index('1')
                    #print('q ', i, ': ', q)
                    codeword_size = 2 * q + 1 + Kexp
                    #print('codeword_size ', i, ': ', codeword_size)
                    N = Exp_golomb_decoding(bitstream[0:codeword_size], Kexp)
                    #print('N ', i, ': ', N)
                    run[i] = N + ((lastRiceQ + 1) * pow(2,Krice))
                    #print('run ', i, ': ',run[i])
                    bitstream = bitstream[codeword_size:]

            ### LEVEL ###
            #print('#### We enter LEVEL ####')
            q = bitstream.index('1')
            #print('q ', i, ': ', q)

            previousLevel = level[i - 1]
            #print('previousLevel ', i, ': ', previousLevel)
            previousLevel = abs(previousLevel) - 1
            #print('|previousLevel| - 1 ', i, ': ', previousLevel)

            lastRiceQ, Krice, Kexp, codebook = define_codebook_level(previousLevel)
            #print('lastRiceQ, Krice, Kexp, codebook ', i, ': ', lastRiceQ, Krice, Kexp, codebook)

            if (codebook == 'exp'):
                #print('#### We enter EXP for LEVEL ####')
                codeword_size = 2 * q + Kexp + 1
                #print('codeword_size ', i, ': ', codeword_size)
                abs_level_minus_1 = Exp_golomb_decoding(bitstream[0:codeword_size], Kexp)
                #print('abs_level_minus_1 ', i, ': ', abs_level_minus_1)
                sign = int(bitstream[codeword_size])
                #print('sign ', i, ': ', sign)
                level[i] = (abs_level_minus_1 + 1) * (1 - 2 * sign)
                #print('level ', i, ': ', level[i])
                bitstream = bitstream[codeword_size + 1:]
                i = i + 1
            elif (codebook == 'combo'):
                if (q <= lastRiceQ):
                    #print('#### We enter COMBO SMALL for LEVEL ####')
                    codeword_size = q + 1 + Krice
                    #print('codeword_size ', i, ': ', codeword_size)
                    abs_level_minus_1 = Golomb_decoding(bitstream[0:codeword_size], Krice)
                    #print('abs_level_minus_1 ', i, ': ', abs_level_minus_1)
                    sign = int(bitstream[codeword_size])
                    #print('sign ', i, ': ', sign)
                    level[i] = (abs_level_minus_1 + 1) * (1 - 2 * sign)
                    #print('level ', i, ': ', level[i])
                    bitstream = bitstream[codeword_size + 1:]
                    i = i + 1
                else:
                    #print('#### We enter COMBO LARGE for LEVEL ####')
                    bitstream = bitstream[lastRiceQ + 1:]
                    q = bitstream.index('1')
                    #print('q ', i, ': ', q)
                    codeword_size = 2 * q + 1 + Kexp
                    #print('codeword_size ', i, ': ', codeword_size)
                    N = Exp_golomb_decoding(bitstream[0:codeword_size], Kexp)
                    #print('N ', i, ': ', N)
                    abs_level_minus_1 = N + ((lastRiceQ + 1) * pow(2,Krice))
                    #print('abs_level_minus_1 ', i, ': ', abs_level_minus_1)
                    sign = int(bitstream[codeword_size])
                    #print('sign ', i, ': ', sign)
                    level[i] = (abs_level_minus_1 + 1) * (1 - 2 * sign)
                    #print('level ', i, ': ', level[i])
                    bitstream = bitstream[codeword_size + 1:]
                    i = i + 1
    return bitstream

def idct_approx(out):
    
    C_norm = 0.3535533905932737
    C_a = 1.387039845322148
    C_b = 1.306562964876377
    C_c = 1.175875602419359
    C_d = 0.785694958387102
    C_e = 0.541196100146197
    C_f = 0.275899379282943
    
    ### process rows
    for i in range(8):

        Y04P = out[i][0] + out[i][4]
        Y2b6eP = C_b * out[i][2] + C_e * out[i][6]

        Y04P2b6ePP = Y04P + Y2b6eP
        Y04P2b6ePM = Y04P - Y2b6eP
        Y7f1aP3c5dPP = C_f * out[i][7] + C_a * out[i][1] + C_c * out[i][3] + C_d * out[i][5]
        Y7a1fM3d5cMP = C_a * out[i][7] - C_f * out[i][1] + C_d * out[i][3] - C_c * out[i][5]

        Y04M = out[i][0] - out[i][4]
        Y2e6bM = C_e * out[i][2] - C_b * out[i][6]

        Y04M2e6bMP = Y04M + Y2e6bM;
        Y04M2e6bMM = Y04M - Y2e6bM;
        Y1c7dM3f5aPM = C_c * out[i][1] - C_d * out[i][7] - C_f * out[i][3] - C_a * out[i][5]
        Y1d7cP3a5fMM = C_d * out[i][1] + C_c * out[i][7] - C_a * out[i][3] + C_f * out[i][5]

        out[i][0] = C_norm * (Y04P2b6ePP + Y7f1aP3c5dPP)
        out[i][7] = C_norm * (Y04P2b6ePP - Y7f1aP3c5dPP)
        out[i][4] = C_norm * (Y04P2b6ePM + Y7a1fM3d5cMP)
        out[i][3] = C_norm * (Y04P2b6ePM - Y7a1fM3d5cMP)

        out[i][1] = C_norm * (Y04M2e6bMP + Y1c7dM3f5aPM)
        out[i][5] = C_norm * (Y04M2e6bMM - Y1d7cP3a5fMM)
        out[i][2] = C_norm * (Y04M2e6bMM + Y1d7cP3a5fMM)
        out[i][6] = C_norm * (Y04M2e6bMP - Y1c7dM3f5aPM)

    ### process columns
    for i in range(8):

        Y04P = out[0][i] + out[4][i]
        Y2b6eP = C_b * out[2][i] + C_e * out[6][i]

        Y04P2b6ePP = Y04P + Y2b6eP
        Y04P2b6ePM = Y04P - Y2b6eP
        Y7f1aP3c5dPP = C_f * out[7][i] + C_a * out[1][i] + C_c * out[3][i] + C_d * out[5][i]
        Y7a1fM3d5cMP = C_a * out[7][i] - C_f * out[1][i] + C_d * out[3][i] - C_c * out[5][i]

        Y04M = out[0][i] - out[4][i]
        Y2e6bM = C_e * out[2][i] - C_b * out[6][i]

        Y04M2e6bMP = Y04M + Y2e6bM;
        Y04M2e6bMM = Y04M - Y2e6bM;
        Y1c7dM3f5aPM = C_c * out[1][i] - C_d * out[7][i] - C_f * out[3][i] - C_a * out[5][i]
        Y1d7cP3a5fMM = C_d * out[1][i] + C_c * out[7][i] - C_a * out[3][i] + C_f * out[5][i]

        out[0][i] = C_norm * (Y04P2b6ePP + Y7f1aP3c5dPP)
        out[7][i] = C_norm * (Y04P2b6ePP - Y7f1aP3c5dPP)
        out[4][i] = C_norm * (Y04P2b6ePM + Y7a1fM3d5cMP)
        out[3][i] = C_norm * (Y04P2b6ePM - Y7a1fM3d5cMP)

        out[1][i] = C_norm * (Y04M2e6bMP + Y1c7dM3f5aPM)
        out[5][i] = C_norm * (Y04M2e6bMM - Y1d7cP3a5fMM)
        out[2][i] = C_norm * (Y04M2e6bMM + Y1d7cP3a5fMM)
        out[6][i] = C_norm * (Y04M2e6bMP - Y1c7dM3f5aPM)
    
    return out

def YUV2RGB(YUV):
    
    # Create LUT for a YUV to RGB conversion of either BT.709 or BT.2020 primaries

    a_2020 = 0.2627
    b_2020 = 0.6780
    c_2020 = 0.0593
    d_2020 = 1.8814
    e_2020 = 1.4747
    
    a = a_2020
    b = b_2020
    c = c_2020
    d = d_2020
    e = e_2020

    RGB = np.empty((1080, 1920, 3))
    
    RGB[:,:,0] = YUV[:,:,0] + e * (YUV[:,:,2] - 0.5)
    RGB[:,:,1] = YUV[:,:,0] - (a * e / b) * (YUV[:,:,2] - 0.5) - (c * d / b) * (YUV[:,:,1] - 0.5)
    RGB[:,:,2] = YUV[:,:,0] + d * (YUV[:,:,1] - 0.5)
    
    return RGB